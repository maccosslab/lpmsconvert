# LPMSConvert #

A [Lakitu](https://bitbucket.org/maccosslab/lakitu) pipeline that runs MSConvert for batch processing.

## Building
```bash
./build.sh release
```

## Testing

```bash
lpmsconvert/interface.py --workers 100 MainTask --run-id smoke_test --ms-set s3://atkeller.lakitu.public/pipeline_tests/test_lpmsconvert/smoke/msfiles.txt --msconvert-config s3://atkeller.lakitu.public/pipeline_tests/test_lpmsconvert/smoke/msconvert_config.txt
```
