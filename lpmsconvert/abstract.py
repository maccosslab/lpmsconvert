import ConfigParser
import StringIO
import os
from datetime import datetime

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws import s3helpers
from lakitu.aws.batch import BatchTask
from lakitu.aws.s3helpers import bs3r, bucket_and_key_from_s3
from lakituapi.pipeline import LakituAwsConfig
from luigi.parameter import ParameterException


def get_log_config():
    """Get the logging configuration for an aws task definition"""
    return {'logDriver': 'awslogs',
            'options': {'awslogs-group': LakituAwsConfig.log_group_name,
                        'awslogs-region': LakituAwsConfig.log_group_region,
                        'awslogs-stream-prefix': "lpmsconvert"}}


class SpecificTaskParameter(luigi.Parameter):
    """
    Parameter corresponding to a specific instantiated task object
    """

    def serialize(self, x):
        return x.task_id


class S3PathParameter(luigi.Parameter):
    """
    Reads in and validates an s3 path
    """

    def parse(self, x):
        if x is None:
            return None
        if not x.startswith("s3://"):
            raise ParameterException("Path must be a valid s3 path or list of s3 paths")
        return x


class RunId(luigi.Config):
    """
    This serves as a unique ID for a run. It determines the working directory where all intermediate files are stored
    for a pipeline. It must be inherited by all tasks in the pipeline in order to prevent the luigi scheduler from
    """
    run_id = luigi.Parameter(default=datetime.now().strftime('%Y-%m-%d_%H-%M-%S'),
                             description="A unique identifier for storing data, including intermediate files and final"
                                         " output. It must be unique")


class BuildToolDirectoryBase(luigi.Task):
    """
    Build a working directory for a tool. Sub directories can be defined using ::
    E.g. to put a tool directory for elib_boundaries under sub directory post processing
    pass toolname as postprocessing::elib_boundaries
    """
    toolname = luigi.Parameter()

    def requires(self):
        """
        Return a dictionary with a 'base_directory' member, e.g.

        {'base_directory': SomeTask}

        where SomeTask gives a single S3Target output
        """
        raise NotImplementedError("Abstract method not defined")

    def run(self):
        s3helpers.mkdir_s3(self.output().path)

    def output(self):
        base_path = self.input()['base_directory'].path
        return s3.S3Target(os.path.join(base_path, *(self.toolname.split('::'))))


class MSConvertBase(BatchTask):
    msc_in_path = S3PathParameter(description="S3 path for input file to convert")
    msc_config = S3PathParameter(description="S3 path to msconvert config")
    _msc_out_format = None

    @property
    def msc_out_format(self):
        """
        Reads the msconvert config file from s3 to get the output file extension
        """
        if self._msc_out_format is None:
            all_exts = ['mzML', 'mzXML', 'mz5', 'mgf', 'text', 'ms1', 'cms1', 'ms2', 'cms2']
            config_str = bs3r.Object(*bucket_and_key_from_s3(self.msc_config)).get()['Body'].read()
            buf = StringIO.StringIO('[global]\n' + config_str)
            config = ConfigParser.ConfigParser()
            config.readfp(buf)
            exts = [x for x in all_exts if x.lower() in [s.lower() for s in config.options('global')]]

            # sanity checking of config file
            if len(exts) == 0:
                self._msc_out_format = 'mzML'  # MSConvert's default when output file extension is not specified
            elif len(exts) == 1:
                self._msc_out_format = exts[0]
            else:
                raise Exception("MSConvert config file must define a single output format (e.g. mzml=True)")

        return self._msc_out_format

    # BatchTask Interface
    job_queue_arn = LakituAwsConfig.linux_job_queue_arn

    @property
    def job_def(self):
        job_def = {
            'jobDefinitionName': 's3wrap_pwiz-skyline_v3_0_19011-ca972c15b_cv1',
            'type': 'container',
            'containerProperties':
                {
                    'image': 'atkeller/s3wrap_pwiz-skyline:v3.0.19011-ca972c15b_cv1',
                    'memory': 7168,  # Allocate at least 2048
                    'vcpus': 4  # allocate at least one core for this
                }
        }
        return job_def
    # /BatchTask Interface

    # AWSTaskBase Interface
    @property
    def command_base(self):
        return ['s3wrap.py', 'wine', 'msconvert', 'Ref::in_path', '-o', 'Ref::out_dir', '--outfile', 'Ref::out_path', '-c', self.msc_config]

    @property
    def parameters(self):
        return {
            'in_path': self.msc_in_path,
            'out_dir': os.path.dirname(self.output().path),
            'out_path': self.output().path,
            'extension': '--' + self.msc_out_format
        }
    # /AWSTaskBase Interface

    # Luigi Interface
    def requires(self):
        """
        Return a dictionary with the following members:

        {
            'msconvert_dir': SomeTask1,  # Task with a .path attribute that is an S3 path where output files can be stored
        }
        """
        raise NotImplementedError("Abstract method not defined")

    def output(self):
        f_bname = os.path.basename(self.msc_in_path)
        f_out_name = os.path.splitext(f_bname)[0] + '.' + self.msc_out_format
        return s3.S3Target(os.path.join(self.input()['msconvert_dir'].path, f_out_name))
    # /Luigi Interface
