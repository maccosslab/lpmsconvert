import StringIO
import logging
import posixpath

import luigi
from lakitu.aws.s3helpers import search_s3_prefix
from luigi.util import inherits

from abstract import S3PathParameter
from copyalltooutputs3dir import CopyAllToOutputS3Dir
from msconvert import MSConvert
from abstract import RunId
from luigi.util import inherits


try:
    from lakitu.aws.s3helpers import read_from_s3
except ImportError:
    from lakitu.aws.s3helpers import bucket_and_key_from_s3, bs3

    def read_from_s3(s3_path):
        """
        Read a file from an S3 source.

        A string of bytes is returned. To read and iterate over a text file, for example, you could do this:

        >>> import StringIO
        >>> msg = read_from_s3('s3://bucket-name/key/foo.txt')
        >>> buf = StringIO.StringIO(msg)
        >>> for line in buf.readline():
        >>>     pass # Do stuff here

        :param s3_path: Path starting with s3://, e.g. 's3://bucket-name/key/foo.txt'
        :return: content : bytes
        """
        bucket, key = bucket_and_key_from_s3(s3_path)
        s3_object = bs3.get_object(Bucket=bucket, Key=key)
        body = s3_object['Body']
        return body.read()


class BatchMSConvertParams(luigi.Config):
    ms_set = S3PathParameter(description="Either"
                                         " (1) an S3 URL pointing to an S3 folder containing MS files,"
                                         " (2) a comma-separated list of S3 URLs pointing to MS files,"
                                         " or (3) an S3 text file with an S3 path to an MS file"
                                         " on each line")
    msconvert_config = S3PathParameter(description="S3 path to msconvert config")


@inherits(RunId)
@inherits(BatchMSConvertParams)
class BatchMSConvert(luigi.WrapperTask):

    def requires(self):
        for m in self._get_ms_paths():
            t = self.clone(MSConvert, msc_in_path=m, msc_config=self.msconvert_config)
            yield t

    def _get_ms_paths(self):
        formats = ("mzml", "mzxml", "mz5", "mgf", "text", "ms2", "cms2", "raw", "d", "wiff")

        if self.ms_set.endswith('.txt'):
            # Read .txt from s3
            buf = StringIO.StringIO(read_from_s3(self.ms_set))
            s3_paths = [list_item.strip() for list_item in buf.readlines()]
        elif self.ms_set.endswith('/'):
            # Get listing of s3 files in this directory
            s3_paths = search_s3_prefix(self.ms_set)
        else:
            # Handle comma-separated list of S3 MS file paths
            s3_paths = [list_item.strip() for list_item in self.ms_set.split(',')]

        return [s3_path for s3_path in s3_paths if posixpath.splitext(s3_path)[-1][1:].lower() in formats]


@inherits(RunId)
@inherits(BatchMSConvertParams)
class MainTask(luigi.WrapperTask):
    def requires(self):
        t = self.clone(BatchMSConvert)
        yield t
        yield CopyAllToOutputS3Dir(run_id=self.run_id, seed_task=t)


if __name__ == '__main__':
    ch = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s %(levelname)8s %(name)s | %(message)s')
    ch.setFormatter(formatter)
    logger = logging.getLogger('lpmsconvert')
    logger.addHandler(ch)
    logger.setLevel(logging.INFO)  # This toggles all the logging in your app
    luigi.run()
