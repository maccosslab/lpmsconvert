from abstract import BuildToolDirectoryBase
from abstract import RunId
from buildworkingdirectory import BuildWorkingDirectory
from luigi.util import inherits


@inherits(RunId)
class BuildToolDirectory(BuildToolDirectoryBase):
    def requires(self):
        return {'base_directory': self.clone(BuildWorkingDirectory)}
