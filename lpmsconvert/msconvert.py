import luigi
from luigi.util import inherits

from abstract import MSConvertBase
from abstract import RunId
from buildtooldirectory import BuildToolDirectory


@inherits(RunId)
class MSConvert(MSConvertBase):
    def requires(self):
        reqs = {"msconvert_dir": self.clone(BuildToolDirectory, toolname="msconvert")}
        return reqs


if __name__ == '__main__':
    luigi.run()
